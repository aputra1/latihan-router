import Table from "./Table"
import Form from "./Form"
import { useState, useEffect } from 'react'
import axios from 'axios'

const Category = () => {
    const [categories, setCategories] = useState([])
    const [refetch, setRefetch] = useState({})

    const storeCategory = async (form) => {
        try {
            const response = await axios.post('http://localhost:3500/api/category/create', form)
            setRefetch(response.data.data)
        } catch (error) {
            console.log(error)
        }
    }

    const getCategoriesData = async () => {
        try {
            const response = await axios.get('http://localhost:3500/api/category/list')
            setCategories(response.data.data)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(
        () => getCategoriesData
    , [refetch])

    return (
        <div className='container'>
            <Form storeCategory={storeCategory}/>
            <br></br>
            <Table categories={categories}/>
        </div>
    )
}

export default Category